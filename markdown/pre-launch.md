Libre-SOC is to be an augmented POWER ISA compliant chip that is
libre-licensed to the
bedrock. It is a low-power, mobile-class, 64-bit Quad-Core SoC at a
minimum 800mhz clock rate, suitable for tablet, netbook, and industrial
embedded systems. Full source code and files are available not only
for the operating system and bootloader, but also for the processor,
its peripherals and its 3D GPU and VPU.

{kazan-logo}

Onboard Libre-SOC is the
[Kazan](https://salsa.debian.org/Kazan-team/kazan) GPU, a
libre-licensed, software-rendered
[Vulkan](https://www.khronos.org/vulkan/) Driver written in
Rust that uses LLVM for code generation.  Kazan will use optimised 3D
instructions specifically designed for and added to Libre-SOC, yet
Kazan itself may still be used (unoptimised) on other hardware.

The performance target for Kazan on Libre-SOC is a very
modest mobile-class level (1280 x 720 25 fps, 100 Mpixels/sec, 30
Mtriangles/sec, 5-6 GFLOPs), whilst the power budget is very tight
(under 2.5 watts in a 28 nm process).
